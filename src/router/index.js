import Vue from "vue";
import VueRouter from "vue-router";
import Qviz from "../views/Qviz.vue";
import MainTeachers from "../views/MainTeachers.vue";
import ResetPassword from "../views/ResetPassword";
import HomePage from "../views/HomePage";
import store from "@/store";
import Api from "@/service/";
Vue.use(VueRouter);
const storeGetters = name => {
  return store.getters[name];
};
const isNotAuthenticated = () => {
  return !localStorage.getItem("user-token");
};
const isCurator = () => {
  return store.getters['user/user_role'] === "curatur";
}
const isStudent = () => {
  return store.getters['user/user_role'] === "student";
}
const errorHandler = ({ type, text }) => {
  store.dispatch("set_loading", false);
  store.dispatch("notifications/add_notification", {
    type,
    text
  });
};
const routes = [
  {
    path: "/verification/:token",
    name: "Verification",
    component: () => import("../views/Verification"),
    meta: {
      layout: "Empty"
    }
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/Login"),
    meta: {
      layout: "Empty"
    }
  },
  {
    path: "/registration",
    name: "Registration",
    component: () => import("../views/Registration"),
    meta: {
      layout: "Empty"
    }
  },
  {
    path: "/home",
    name: "HomePage",
    component: HomePage,
    beforeEnter(from, to, next) {
      if (isNotAuthenticated()) {
        next("/login");
        return;
      }
      store.dispatch("set_loading", true);
      store
        .dispatch("user/get_lessons")
        .then(() => {
          store.dispatch("set_loading", false);
          store.dispatch("get_types")
          store.dispatch("user/get_notifications")
            .then(() => {
              next();
            })
        })
        .catch(() => {
          store.dispatch("auth/logout").then(() => {
            next({ name: "Login" });
            errorHandler({
              type: "danger",
              text: "Возникла ошибка! Попробуйте еще раз через несколько минут."
            });
          });
        });
    }
  },
  {
    path: "/quiz",
    name: "Qviz",
    component: Qviz,
    meta: {
      layout: "Empty"
    }
  },
  {
    path: "/",
    name: "Teachers",
    component: MainTeachers,
    props: true,
    beforeEnter(to, from, next) {
      if (isNotAuthenticated()) {
        next("/login");
        return;
      }
      if(isCurator()) {
        next({ name: 'HomePage' })
        return;
      }
      store.dispatch("set_loading", true);
      if (storeGetters("curators/curators_array")) {
        to.params.curators = storeGetters("curators/curators_array");
        store.dispatch("set_loading", false);
        next();
        return;
      }
      store
        .dispatch("curators/get_curators")
        .then(() => {
          to.params.curators = storeGetters("curators/curators_array");
          store.dispatch("set_loading", false);
          next();
        })
        .catch(() => {
          errorHandler({
            type: "danger",
            text: "Возникла ошибка! Попробуйте еще раз через несколько минут."
          });
          next({ name: "HomePage" });
        });
    }
  },
  {
    path: "/teacher/:id",
    name: "TeacherPage",
    props: true,
    component: () => import("../views/TeacherPage"),
    beforeEnter(to, from, next) {
      if (isNotAuthenticated()) {
        next("/login");
        return;
      }
      if(isCurator()) {
        next({ name: 'HomePage' })
        return;
      }
      store.dispatch("set_loading", true);
      Api.get_curator_by_id(to.params.id)
        .then(({ data: { teachers } }) => {
          to.params.curator = teachers;
          store.dispatch("set_loading", false);
          next();
        })
        .catch(() => {
          errorHandler({
            type: "danger",
            text: "Возникла ошибка! Попробуйте еще раз через несколько минут."
          });
          next({ name: "Teachers" });
        });
    }
  },
  {
    path: "/level",
    name: "FindOutLevel",
    component: () => import("../views/FindOutLevel"),
    beforeEnter(from, to, next) {
      if (isNotAuthenticated()) {
        next('/login')
        return
      }
      if(isCurator()) {
        next({ name: 'HomePage' })
        return;
      }
      next()
    }
  },
  {
    path: "/reset-password/:token",
    name: "ResetPassword",
    component: ResetPassword,
    meta: {
      layout: "Empty"
    }
    // beforeEnter(from, to, next) {
    //   if (isNotAuthenticated()) {
    //     next('/login')
    //     return
    //   }
    //   next()
    // }
  },
  {
    path: "/password-recovery/",
    name: "PasswordRecovery",
    component: () => import("../views/PasswordRecovery"),
    meta: {
      layout: "Empty"
    }
  },
  {
    path: "/help",
    name: "Help",
    props: true,
    component: () => import("../views/Help"),
    beforeEnter(to, from, next) {
      if (isNotAuthenticated()) {
        next("/login");
        return;
      }
      if(isCurator()) {
        next({ name: 'HomePage' })
        return;
      }
      store.dispatch("set_loading", true);
      Api.get_faq_questions()
        .then(({ data: { faq } }) => {
          to.params.questions = faq;
          store.dispatch("set_loading", false);
          next();
        })
        .catch(() => {
          errorHandler({
            type: "danger",
            text: "Возникла ошибка! Попробуйте еще раз через несколько минут."
          });
          next({ name: "HomePage" });
        });
    }
  },
  {
    path: "/my-message",
    name: "MyMessage",
    props: true,
    component: () => import("../views/MyMessage"),
    beforeEnter(to, from, next) {
      if (isNotAuthenticated()) {
        next("/login");
        return;
      }
      if(isCurator()) {
        next({ name: 'HomePage' })
        return;
      }
      store.dispatch("set_loading", true);
      // if(storeGetters('user/user_tickets')) {
      //   to.params.tickets = storeGetters('user/user_tickets');
      //   store.dispatch('set_loading', false);
      //   next()
      //   return
      // }
      store
        .dispatch("user/get_tickets")
        .then(() => {
          next();
          store.dispatch("set_loading", false);
        })
        .catch(() => {
          next({ name: "HomePage" });
          errorHandler({
            type: "danger",
            text: "Возникла ошибка! Попробуйте еще раз через несколько минут."
          });
        });
    }
  },
  {
    path: "/checkout/:id",
    name: "Checkout",
    component: () => import("../views/CheckoutPage"),
    beforeEnter(from, to, next) {
      if (isNotAuthenticated()) {
        next('/login')
        return
      }
      if(isCurator()) {
        next({ name: 'HomePage' })
        return;
      }
      next()
    }
  },
  {
    path: "/purchase-success",
    name: "PurchaseSuccess",
    component: () => import("../views/PurchaseSuccess"),
    beforeEnter(from, to, next) {
      if (isNotAuthenticated()) {
        next('/login')
        return
      }
      next()
    }
  },
  {
    path: "/promotion-lesson",
    name: "PromotionLesson",
    component: () => import("../views/PromotionLesson"),
    beforeEnter(from, to, next) {
      if (isNotAuthenticated()) {
        next('/login')
        return
      }
      if(isCurator()) {
        next({ name: 'HomePage' })
        return;
      }
      next()
    }
  },
  {
    path: "/offer",
    name: "SpecialOfferPage",
    component: () => import("../views/SpecialOfferPage"),
    beforeEnter(from, to, next) {
      if (isNotAuthenticated()) {
        next('/login')
        return
      }
      if(isCurator()) {
        next({ name: 'HomePage' })
        return;
      }
      next()
    }
  },
  {
    path: "/settings",
    name: "SettingPage",
    component: () =>
      import(/* webpackChunkName: 'settings-page' */ "@/views/SettingPage"),
    beforeEnter(from, to, next) {
      if (isNotAuthenticated()) {
        next('/login')
        return
      }
      if(isCurator()) {
        next({ name: 'HomePage' })
        return;
      }
      next()
    }
  },
  {
    path: "/teacher-timeline",
    name: "TeacherTimeLine",
    component: () => import("@/views/TeacherTimeLine.vue"),
    beforeEnter(from, to, next) {
      if (isNotAuthenticated()) {
        next('/login')
        return
      }
      if(isStudent()) {
        next({ name: 'HomePage' })
        return;
      }
      next()
    }
  },
  {
    path: "/calendar",
    name: "Calendar",
    component: () =>
      import("@/views/Calendar"),
    beforeEnter(from, to, next) {
      if (isNotAuthenticated()) {
        next('/login')
        return
      }
      if(isCurator()) {
        next({ name: 'HomePage' })
        return;
      }
      next()
    }
  },
  {
    path: "/new-lessons",
    name: "NewTeacherLessons",
    component: () => import("@/views/NewTeacherLessons"),
    beforeEnter(from, to, next) {
      if (isNotAuthenticated()) {
        next('/login')
        return
      }
      if(isStudent()) {
        next({ name: 'HomePage' })
        return;
      }
      next()
    }
  },
  {
    path: "/students-homework",
    name: "TeacherHomeWork",
    component: () =>
      import("@/views/TeacherHomeWork"),
    beforeEnter(from, to, next) {
      if (isNotAuthenticated()) {
        next('/login')
        return
      }
      if(isStudent()) {
        next({ name: 'HomePage' })
        return;
      }
      next()
    }
  },
  {
    path: "/students",
    name: "StudentsPage",
    component: () => import("@/views/Students"),
    beforeEnter(from, to, next) {
      if (isNotAuthenticated()) {
        next('/login')
        return
      }
      if(isStudent()) {
        next({ name: 'HomePage' })
        return;
      }
      next()
    }
  }
];

const router = new VueRouter({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes
});
//to, from, next
router.beforeEach((to, from, next) => {
  // store.dispatch('set_loading', true)
  store.dispatch("hide_modal");
  next();
});
router.afterEach(() => {
  // store.dispatch('set_loading', false)
  // store.dispatch('hide_show_overlay', true)
});
export default router;
