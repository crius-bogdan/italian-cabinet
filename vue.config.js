module.exports = {
  publicPath: process.env.NODE_ENV === "production" ? "" : "",
  css: {
    sourceMap: true,
    loaderOptions: {
      sass: {
        prependData: `@import "~@/assets/scss/_includes.scss";`
      }
    }
  }
};
