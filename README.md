# italian-cabinet

## Первое что тебе надо сделать
Установить node.js [клик](https://nodejs.org/en/) скачать v 12.16.1
Установить git bash [клик](https://git-scm.com/downloads)

Открыть `git bash` (нажать правую кнопку мыши и выбрать пункт git bash here)
Установить VUE.js глобально
```
npm i -g @vue/cli
```
Копировать репозиторий к себе на локалку
Перейти в папку в которой у тебя будет проект открыть git bash 
Вставить команду и нажать enter
```
git clone https://gitlab.com/crius-bogdan/italian-cabinet.git
```
И запустить следующие команды
```
cd italian-cabinet
git checkout dev-html-css
npm install
npm run serve
```
По очереди запускаешь команды и ждешь пока они выполнятся
когда ты запустишь последнюю команду ты уже можешь начинать работать, ты запустишь локальный сервер в котором будет автоматическая перезагрузка после того как ты изменишь какой-то файл и будет показываться ошибка если ты сделаешь что то не так. Ошибки чаще всего будут показываться в консоли в которой ты запустил команду `npm run serve`

# !!!Важные моменты
Стили для текстов брать здесь `src/assets/scss/base/_fonts.scss`
Это работает так ты в фигме нажимаешь на текст и можешь увидеть данную панель [клик](https://www.dropbox.com/s/1ykgzvq0boqr727/%D0%A1%D0%BA%D1%80%D0%B8%D0%BD%D1%88%D0%BE%D1%82%202020-03-06%2010.18.17.png?dl=0) 
Я кругами выделил то на что тебе надо смотреть 
а теперь мы переходим в файл `src/assets/scss/base/_fonts.scss` и по коментариям ищем похожее название [клик](https://www.dropbox.com/s/rfxlsi932v57lcm/%D0%A1%D0%BA%D1%80%D0%B8%D0%BD%D1%88%D0%BE%D1%82%202020-03-06%2010.22.32.png?dl=0)
Копируем то что выделено синим цветом потом идем в код где ты допустим сейчас стилизуешь этот заголовок и прописываес ему
```SCSS
.title__class-here {
    @extend %h1-title;
    color: $black;
    //... и другие нстройки для этого элемента
}
```

## Полезные классы 
Находятся здесь `src/assets/scss/helpers/_extends.scss`
Тебе надо просто добавить его к элементу в котором должно быть то или иное свойство 
таким образом ->
```SCSS
.class__name-here {
    @extend %df; // display: flex;
    @extend %aic; // align-items: center;
    @extend %jcsb; // justify-content: space-between;
}
```

## Как использовать спрайт иконки

В места где тебе нужна иконка пишешь так
```PUG
BaseIcon(
    name="icon-name" 
    width="ширина иконки" 
    height="высота иконки" 
    mod="если надо добавляешь это здесь типа специальный класс чтобы можно было навешать какие то уникальные стили на иконку допустим цвет"
    )
```
а имя ты можешь узнать из макета у него должно быть логичное имя и выглядит оно примерно так `icons/название-иконки`

## Переменные с цветами 
Здесь `src/assets/scss/helpers/_variables.scss` там все цвета которые тебе понадобятся
